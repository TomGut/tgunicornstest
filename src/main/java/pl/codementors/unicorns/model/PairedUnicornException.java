package pl.codementors.unicorns.model;

public class PairedUnicornException extends UnicornException {

    public PairedUnicornException() {
    }

    public PairedUnicornException(String message) {
        super(message);
    }

    public PairedUnicornException(String message, Throwable cause) {
        super(message, cause);
    }

    public PairedUnicornException(Throwable cause) {
        super(cause);
    }

    public PairedUnicornException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
